import React, { Component } from 'react';
import './App.css';



import Burger from './components/Burger/Burger'
import Buttons from './components/Buttons/Buttons'


class App extends Component {
  state = {
    ingredients: [
        {name: 'Meat',count : 0, id: 1},
        {name: 'Cheese',count : 0, id:2},
        {name: 'Salad',count : 0, id: 3},
        {name: 'Bacon',count : 0, id:3}
    ]
  };


addIngredient = (name) => {
    let ingredients = [...this.state.ingredients] ;
    for(let i = 0; i < ingredients.length; i ++) {
        if(name === ingredients[i].name) {
            ingredients[i].count++
        }
    }
    this.setState({
        ingredients: ingredients
    })
};

removeIngredient = (name) => {
    let ingredients = [...this.state.ingredients] ;
    for(let i = 0; i < ingredients.length; i ++) {
        if(name === ingredients[i].name && ingredients[i].count > 0) {
            ingredients[i].count--
        }

    }
    this.setState({
        ingredients: ingredients
    })
};


  render() {
    return (

      <div className="App">

        <div className="item">
         <Buttons ingredients={this.state.ingredients} click={(name) => this.addIngredient(name)} removeClick={(name) => this.removeIngredient(name)} />
        </div>
        <div className="item">

            <Burger ingredients={this.state.ingredients}/>
            <p>price:</p>
        </div>

      </div>
    );
  }
}

export default App;
