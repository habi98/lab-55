import React from 'react';
import Ingrediennt from '../Ingredients/Ingredient';
import './Burger.css';

const Burger = (props) => {
    return(
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {props.ingredients.map((ingredient, index) => {
                return (
                    <Ingrediennt key={index} count={ingredient.count} name={ingredient.name}/>
                )
            })}
            <div className="BreadBottom"></div>
        </div>
    )
};

export default Burger;
