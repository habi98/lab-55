import React from 'react';
import './Buttons.css';

import Meat from '../assets/steak.png';
import Cheese from  '../assets/cheese.png';
import Salad from '../assets/salad.png'
import Becon from '../assets/bacon.png'
import Dusbin from '../assets/dustbin.png'

const INGREDIENTS = [
    {name: 'Meat', price: 50, img: Meat},
    {name: 'Cheese', price: 20, img: Cheese},
    {name: 'Salad', price: 5, img: Salad},
    {name: 'Bacon', price: 30, img: Becon}
];


const getCountIngredient = (props, ingredientName) => {
    let id = props.ingredients.findIndex((ingredient) => {
        return ingredient.name === ingredientName
    });
    return props.ingredients[id].count;
};

const Buttons = (props) => {
    return(
        <div>
            {INGREDIENTS.map((ingredient, id) => {
                return (
                    <p key={id}>
                        <span><img src={ingredient.img} alt=""/></span>
                        <span>
                    <button type="button" className="btn-ingre" onClick={() => props.click(ingredient.name)}>{ingredient.name}</button>
                </span>x {getCountIngredient(props, ingredient.name)}<span><button type="button" className="btn-ingre" id='removeBtn' onClick={() => props.removeClick(ingredient.name)}><img src={Dusbin} alt=""/></button></span></p>
                )
            })}
        </div>
    )
};


export default Buttons;