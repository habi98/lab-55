import React from 'react';

import './Ingredient.css';

const Ingredient = (props) => {

    const getIngredients = () => {
        let names = [];
        for(let i = 0; i < props.count; i++) {
            names.push(props.name);
        }
        return names;
    };

    return(
      getIngredients().map((ingredientName, index) => {
          return (
              <div key={index} className={`${ingredientName}`}></div>
          )
      })
    )
};

export default Ingredient;
